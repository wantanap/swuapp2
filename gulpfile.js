var gulp = require('gulp');

var minJs =require('gulp-uglify');
var ren =require('gulp-rename');
var sass =require('gulp-ruby-sass');

var libjs=[
    "assets/jquery/dist/jquery.min.js",
    "assets/bootstrap/dist/js/bootstrap.min.js"
];

var libcss=[
        "assets/bootstrap/dist/css/bootstrap.min.css"
];

gulp.task ('js',function(){
    return gulp
    .src('assets/mylib/js/script.js')    
    .pipe(minJs())
    .pipe(ren({suffix: '.min'}))
    .pipe(gulp.dest('wwwroot/js'));
});



gulp.task ('css',function(){
    sass('assets/mylib/css/style.scss',{
        style: 'compressed'

    })
    .on('error',sass.logError)
    .pipe(ren({suffix: '.min'}))
    .pipe(gulp.dest('wwwroot/css'));
});


//copy library js ของ  jquery และ bootstrap
gulp.task('copylibjs',function(){
    return gulp
    .src(libjs)
    .pipe(gulp.dest('wwwroot/js'));

});

//copy library css ของ  css และ bootstrap
gulp.task('copylibcss',function(){
    return gulp
    .src(libcss)
    .pipe(gulp.dest('wwwroot/css'));

});
gulp.task('all',['copylibjs','copylibcss','css','js']);
